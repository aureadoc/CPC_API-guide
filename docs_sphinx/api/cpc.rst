.. _api_functions:


Library information
===================

CPC_getLibVersion
-----------------

.. doxygenfunction:: CPC_getLibVersion
   :project: CPC - API guide




Connection Functions
====================

CPC_listDevices
---------------

.. doxygenfunction:: CPC_listDevices
   :project: CPC - API guide

CPC_openDevice
--------------

.. doxygenfunction:: CPC_openDevice
   :project: CPC - API guide

CPC_closeDevice
---------------

.. doxygenfunction:: CPC_closeDevice
   :project: CPC - API guide





Save and Reset Settings
=======================

CPC_saveAllSettings
-------------------

.. doxygenfunction:: CPC_saveAllSettings
   :project: CPC - API guide

CPC_factorySettings
-------------------

.. doxygenfunction:: CPC_factorySettings
   :project: CPC - API guide





Reboot System
=============

CPC_resetSystem
---------------

.. doxygenfunction:: CPC_resetSystem
   :project: CPC - API guide





Device Information
==================

CPC_getSystemVersion
--------------------

.. doxygenfunction:: CPC_getSystemVersion
   :project: CPC - API guide

CPC_getSystemFeature
--------------------

.. doxygenfunction:: CPC_getSystemFeature
   :project: CPC - API guide

CPC_getSystemHardwareVersion
----------------------------

.. doxygenfunction:: CPC_getSystemHardwareVersion
   :project: CPC - API guide





Recover Parameters Range
========================

CPC_getEfficiencyRange
----------------------

.. doxygenfunction:: CPC_getEfficiencyRange
   :project: CPC - API guide

CPC_getDeadTimeRange
--------------------

.. doxygenfunction:: CPC_getDeadTimeRange
   :project: CPC - API guide




Set and Get Parameters
======================

CPC_setEfficiency
-----------------

.. doxygenfunction:: CPC_setEfficiency
   :project: CPC - API guide

CPC_getEfficiency
-----------------

.. doxygenfunction:: CPC_getEfficiency
   :project: CPC - API guide

CPC_setDeadTime
---------------

.. doxygenfunction:: CPC_setDeadTime
   :project: CPC - API guide

CPC_getDeadTime
---------------

.. doxygenfunction:: CPC_getDeadTime
   :project: CPC - API guide

CPC_setCountingRate
-------------------

.. doxygenfunction:: CPC_setCountingRate
   :project: CPC - API guide

CPC_getCountingRate
-------------------

.. doxygenfunction:: CPC_getCountingRate
   :project: CPC - API guide

CPC_setOutputState
------------------

.. doxygenfunction:: CPC_setOutputState
   :project: CPC - API guide

CPC_setIntegTime
----------------

.. doxygenfunction:: CPC_setIntegTime
   :project: CPC - API guide

CPC_getIntegTime
----------------

.. doxygenfunction:: CPC_getIntegTime
   :project: CPC - API guide

CPC_setAnalogOutGain
--------------------

.. doxygenfunction:: CPC_setAnalogOutGain
   :project: CPC - API guide

CPC_getAnalogOutGain
--------------------

.. doxygenfunction:: CPC_getAnalogOutGain
   :project: CPC - API guide

CPC_setDetectionMode
--------------------

.. doxygenfunction:: CPC_setDetectionMode
   :project: CPC - API guide

CPC_getDetectionMode
--------------------

.. doxygenfunction:: CPC_getDetectionMode
   :project: CPC - API guide

CPC_setInputVoltageThreshold
----------------------------

.. doxygenfunction:: CPC_setInputVoltageThreshold
   :project: CPC - API guide

CPC_getInputVoltageThreshold
----------------------------

.. doxygenfunction:: CPC_getInputVoltageThreshold
   :project: CPC - API guide

CPC_getDetLimitThreshold
------------------------

.. doxygenfunction:: CPC_getDetLimitThreshold
   :project: CPC - API guide





Monitoring Functions
====================

CPC_getCLKCountData
-------------------

.. doxygenfunction:: CPC_getCLKCountData
   :project: CPC - API guide

CPC_getBodySocketTemp
---------------------

.. doxygenfunction:: CPC_getBodySocketTemp
   :project: CPC - API guide

CPC_getOutputVoltage
--------------------

.. doxygenfunction:: CPC_getOutputVoltage
   :project: CPC - API guide

CPC_getSystemAlarms
-------------------

.. doxygenfunction:: CPC_getSystemAlarms
   :project: CPC - API guide





Hardware Control
================

CPC_setFanState
---------------

.. doxygenfunction:: CPC_setFanState
   :project: CPC - API guide

CPC_getFanState
---------------

.. doxygenfunction:: CPC_getFanState
   :project: CPC - API guide

CPC_setLedState
---------------

.. doxygenfunction:: CPC_setLedState
   :project: CPC - API guide

CPC_getLedState
---------------

.. doxygenfunction:: CPC_getLedState
   :project: CPC - API guide


CPC_identify
------------

.. doxygenfunction:: CPC_identify
   :project: CPC - API guide