.. _custom_application:

Custom Application
==================

The following section guides you through the developement of your own application.

Windows
-------

Requirements
~~~~~~~~~~~~

   Software : Visual Studio (2019)

   Visual Studio extensions :

      - Desktop Development C++

      - Development Python


Create C++ application using the existing Visual Studio project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   - Locate the CPC-API folder and go to "CPC-API/Applications/".
   - Open "CPC_Solution.sln".

   .. figure:: /pictures/picture_windows_1.PNG

   - Three differents C++ programs have been developed to help you, to change the selected applications right click on the desired example and select "Set as Startup Project". 

   .. figure:: /pictures/picture_windows_2.PNG

   - To test those examples, make sure you have selected "Release" and "x64", and click on "Run".

   .. figure:: /pictures/picture_windows_3.PNG

   - To create your own application click on "File" > "New" > "Project..." Then select "Console App", choose an application name, select "Add to solution" and click on "Create".

   .. figure:: /pictures/picture_windows_4.PNG

   - Now, you need to configure your project, right click on it and then click on "Properties".

   .. figure:: /pictures/picture_windows_5.PNG

   - Then make sure Configuration section is set to "All Configurations" and Platform is set to "x64".
   - Go to "Configuration Properties" > "General" > "General Properties" and set the "Output Directory" to "$(SolutionDir)\\$(ProjectName)\\bin\\$(Platform)\\$(Configuration)\\". Then set the "Intermediate Directory" to "build\\$(Platform)\\$(Configuration)\\".

   .. figure:: /pictures/picture_windows_6.PNG

   - Go to "Configuration Properties" > "C/C++" > "General" and set the "Additional Include Directories" to "$(SolutionDir)\\..\\Sources\\h".

   .. figure:: /pictures/picture_windows_7.PNG

   - Go to "Configuration Properties" > "Linker" > "General" and set the "Additional Library Directories" to "$(SolutionDir)\\..\\Sources\\bin\\$(Platform)".

   .. figure:: /pictures/picture_windows_8.PNG

   - Go to "Configuration Properties" > "Linker" > "Input" and set the "Additional Dependencies" to "CPC.lib".

   .. figure:: /pictures/picture_windows_9.PNG

   - Finally, in the goal to locally run the application, add the copy of the library on the output folder. Go to "Configuration Properties" > "Build Events" > "Pre-Build Event" and set the "Command Line" to "xcopy "$(SolutionDir)\\..\\Sources\\bin\\$(Platform)\\*.dll" "$(OutDir)" /R /Y /E".

   .. figure:: /pictures/picture_windows_10.PNG

   .. note::
      If you use windows 7, the xcopy command may give you an error. To solve this issue, add this path to your environement path : "C:\Windows\System32". You also can remove the xcopy command and manually copy the CPC.dll file in the same directory as your application executable file.

   Your C++ application is now ready to use. Please refer to Section :ref:`Code Examples` to access basic code.

Create Python application using the existing Visual Studio project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   - Locate the CPC-API folder and go to "CPC-API/Applications/".
   - Open "CPC_Solution.sln".

   .. figure:: /pictures/picture_windows_1.PNG

   - One Python program has been developed to help you, to change the selected applications right click on the desired example and select "Set as Startup Project". 

   .. figure:: /pictures/picture_windows_11.PNG

   - To create your own application click on "File" > "New" > "Project..." then select "Python Application", choose an application name, select "Add to solution" and click on "Create".

   .. figure:: /pictures/picture_windows_12.PNG

   - Now, you need to configure your project, right click on it and then click on "Properties".

   .. figure:: /pictures/picture_windows_13.PNG

   - In order to not locally copy the library, you can adjust the "Working Directory" with the library path "../../Sources/bin/x64"

   .. figure:: /pictures/picture_windows_14.PNG

   - In "Debug" set the "Search Paths" to "..\\..\\Sources\\wrapper" allowing to specify the wrapper package location 

   .. figure:: /pictures/picture_windows_15.PNG

   - Finally, right click on "Search Paths" and select "Add Folder to Search Path...".

   .. figure:: /pictures/picture_windows_16.PNG

   - Then locate and select "CPC-API/Sources/wrapper".

   .. figure:: /pictures/picture_windows_17.PNG

   Your Python application is now ready to use. Please refer to Section :ref:`Code Examples` to access basic code.

MacOS
-----

Requirements
~~~~~~~~~~~~

   Software : XCode.


Create C++ application using the existing Xcode project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   - Locate the CPC-API folder and go to "CPC-API/Applications/C++/".
   - Open "Applications.xcodeproj".

   .. figure:: /pictures/picture_mac_1.PNG

   - Three differents programs have been developed to help you, to change the selected applications click on the list at the top.

   .. figure:: /pictures/picture_mac_2.PNG

   - To create your own application click on "File" > "New" > "Target..."" Then select Command Line Tools, choose an application name and click on "Finish".

   .. figure:: /pictures/picture_mac_3.PNG

   - Now, you need to configure your target, click on it, click on "General", on "Frameworks and Libraries" and click "+".
   - Click on "Add Ohter..." and select "Add Files". Then add libCPC.dylib that is locate in "CPC-API/Sources/bin/".

   .. figure:: /pictures/picture_mac_4.PNG

   - To avoid library issue, click on your target, click on “Signing & Capabilities”, check box “Disable Library Validation”.

   .. figure:: /pictures/picture_mac_5.PNG

   Your C++ application is now ready to use. Please refer to Section :ref:`Code Examples` to access basic code.

Create Python application
~~~~~~~~~~~~~~~~~~~~~~~~~

   - Locate the CPC-API folder and go to "CPC-API/Applications/Python/". 
   - An Example has been developed to help you, to develop your own, we advise you to copy AllFunctionsCrtl.py, rename it and make your modifications. Please make sure you have CPC software installed and you are using CPC_wrapper.py file.

   Your Python application is now ready to use. Please refer to Section :ref:`Code Examples` to access basic code.

Linux
-----

Requirements
~~~~~~~~~~~~

   Package : build-essential, libudev-dev

   To install these packages, please execute the following commands :

   .. code-block:: console
   
      sudo apt update
      sudo apt upgrade
      sudo apt install build-essential
      sudo apt install libudev-dev
      sudo apt install libusb-1.0-0-dev

   .. note::
      If you have installed the Aurea-CPS Software, you may already have the necessary packages.

Makefile Example
~~~~~~~~~~~~~~~~

   - Locate the CPC-API folder and go to "CPC-API/Applications/C++".
   - Three differents programs have been developed to help you, To create your own application, we advise you to copy the AllFunctionsCrtl folder. Then rename the folder and the AllFunctionCrtl.cpp file. Finally replace the target name by your application name in the Makefile.

   .. figure:: /pictures/picture_linux_1.PNG

   - Finally edit the cpp file to develop your application.

Create Python application
~~~~~~~~~~~~~~~~~~~~~~~~~

   - Locate the CPC-API folder and go to "CPC-API/Applications/Python/". 
   - An Example has been devloped to help you, to develop your own, we advise you to copy AllFunctionsCrtl.py, rename it and make your modifications. Please make sure you have CPC software installed and you are using CPC_wrapper.py file.

   Your Python application is now ready to use. Please refer to Section :ref:`Code Examples` to access basic code.