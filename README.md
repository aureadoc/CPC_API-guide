# DOCUMENTATION

## Requirements

### Common

#### Install GitBash (for Windows, allowings to run followings commands) :
	browse to git website and download the application
#### Install Miktex (for Windows) or MacTex (for MacOS) : 
	browse to miktek website and download the application
#### Install LatexMk (for Linux) : 
	sudo apt install latexmk && sudo apt install texlive-latex-extra

### DOXYGEN
	- Browse to doxygen website and download doxygen-version-setup.exe
	- Add Doxygen to the environement path
	- Install (for Linux) : sudo apt install doxygen

### SHPINX

#### Install Python3 & pip :
	Linux: sudo apt-get install python3 python3-pip
	MacOS: brew install python
#### Update pip : 
	pip install --upgrade pip
#### Add pip to the environement path (if you don't use gitBash) 
	Linux: export PATH="$HOME/<pathToAdd>:$PATH"
#### Install Sphinx, by the command :
	Linux: pip install -U sphinx   
#### add Sphinx modules :
	pip install sphinx-sitemap
	pip install sphinx-rtd-theme
#### Install Breathe :
	pip install breathe
#### On Windows, add following environement path :
	python39\site-packages
	Python39\Scripts

## To Generate Doxygen documentation

Go to docs_doxygen

### HTML

#### Execute the following command : 
	"doxygen Doxyfile.in" or open "Doxyfile.in" with DoxygenWizard

Documentation version must be change manually in "header.sty"

Documentation will be generated in \_build/html/

### PDF

#### To generate documentation in pdf :
	for Windows: execute make.bat file in \_build/latex/make.bat

## To Generate ReadTheDocs documentation

Go to docs_sphinx

### PDF

#### On MacOS or on Linux, execute the following command : 
	make latexpdf

#### On windows, execute the following commands (from Git Bash) : 
	sphinx-build -b latex . \_build/latex
	pdflatex \_build/latex/CPC_API-guide.tex -output-directory \_build/latex

Documentation will be generated in \_build/latex/

### HTML

#### On MacOS or on Linux, execute the following command : 
	make html
#### On windows, execute the following command (from Git Bash) : 
	sphinx-build -b html . \_build/html

Documentation will be generated in \_build/html/

## Notes

To update only documentation, you need to push changes on gitlab : git@gitlab.com:aureadoc/CPC_API-guide.git

Then readthedocs will automatically generate new documentation, Pdf format is available directly on readTheDocs

## Update documentation

- Update CPC.h file in include directory

- Update cpc.rst file (if functions added or name updated) in docs_sphinx/api directory

- Update verison in file conf.py in docs_sphinx directory

- Update revision.rst in docs_sphinx/revision directory
