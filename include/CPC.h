////////////////////////////////////////////////////////////////////////////// 
/// 
/// \mainpage			CPC Library
///
///--------------------------------------------------------------------------- 
/// \section det_sec Details
/// - FileName		:	CPC.h
/// - Dependencies	:   None
/// - Compiler		:   C/C++
/// - Company		:   Copyright (C) Aurea Technology
/// - Author		:	Matthieu Grovel
///
///	\section des_sec Description
///	This header document provide the prototypes and descriptions of all functions    
///	integrated on both dynamic (.dll) and static libraries (.lib) files.
/// Those functions allows to control CPC device also named "CPC"
/// 
/// \section imp_sec Important
///
/// More or less functions are available according to the device type.	
///	The compatibility depends on the device part number recovered by
///	the "GetSystemVersion" function.								
///	Please see notes functions to check the compatibility with your device:  	
///   -> version compatibility:  PN_CPC_x_xx_xx_xx 	
/// 
/// \section rev_sec Revisions
///
/// v1.0 (21/05/14)
/// - First release
///
/// v2.0 (22/09/14)	
/// - Add functions:
/// 	- SetOutputFormat()
/// 	- GetOutputFormat()
/// 	- SetIntegTime()
/// 	- GetIntegTime()
/// 	- SetAnalogOutGain()
/// 	- GetAnalogOutGain()
///
/// v3.0 (19/03/15)	
/// - Replace functions:
/// 	- OpenSystemTransfer() to OpenATdevice()
/// 	- CloseSystemTransfer() to CloseATdevice()
///
/// v3.1 (23/12/16)
/// - Improvement GetCLKCountData() function
/// - Add functions:
/// 	- GetOutputVoltage()
///
/// v3.2 (11/01/17)	
/// - Internal improvement
///
/// v3.3 (12/01/17)	
/// - Improvement of internals functions
///    to close the DLL cleaner way 
///
/// v3.4 (02/08/17)	
/// - Improvement of the device closing
///
/// v3.5 (26/02/19)
/// - Improvement of functions:
/// 	- setDeadtime() (accuracy to the thousandth)
///
/// v3.6 (27/02/20)	
/// - Rename functions:
/// 	- SetVisibleModuleDetection() to SetDetectionMode()
/// 	- GetVisibleModuleDetection() to GetDetectionMode()
/// 
/// v3.7 (22/06/21)	
/// - Modification functions:
///		- SetOutputFormat(): add NIM format 
///		- GetOutputFormat(): add NIM format
/// - Modifications of function compatibilities and comments
///		
/// 
/// v4.0 (16/07/21)
/// - Add Device index in all functions
/// - Modify all get functions to return value by pointer
/// - Manage multiple device utilization in the same program
/// - Add CPC_ at the beginning of all functions
/// - Replace functions:
/// 	- CPC_listATdevices() to CPC_listDevices()
/// 	- CPC_openATdevice() to CPC_openDevice()
///		- CPC_closeATdevice() to CPC_closeDevice()
////////////////////////////////////////////////////////////////////////////// 

#ifndef _CPC_H
#define _CPC_H

#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Define LIB_CALL for any platform
#if defined _WIN32 || defined __CYGWIN__
	#if defined WIN_EXPORT
		#ifdef __GNUC__
		#define LIB_CALL __attribute__ ((dllexport))
		#else
		#define LIB_CALL __declspec(dllexport) __cdecl
		#endif
	#else
		#ifdef __GNUC__
		#define LIB_CALL __attribute__ ((dllimport))
		#else
		#define LIB_CALL __declspec(dllimport) 
		#endif
	#endif
#else
	#if __GNUC__ >= 4
	#define LIB_CALL __attribute__ ((visibility ("default")))
	#else
	#define LIB_CALL
	#endif
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include "conio.h"
#define delay(x) Sleep(x)
#elif __unix
#include <cstring>
#include <unistd.h>
#define delay(x) usleep(x*1000)
#else
#include <unistd.h>
#define delay(x) usleep(x*1000)
#endif

#ifdef _WIN32 
#define secure_strtok strtok_s
#else
#define secure_strtok strtok_r
#endif	

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#if defined(__cplusplus)
extern "C" {
#endif

 /////////////////////////////////////////////////////////////////////////////////////
/// \fn			short CPC_getLibVersion(unsigned short *value)
///	\brief		Get the librarie version
/// \details	Return the version librarie in format 0x0000MMmm \n
///				with: MM=major version \n
///					  mm=minor version			  
///
/// \param		*value	return lib version by pointer \n
///						Format: 0xMMmm		\n
///						with:	MM: major version	\n
///								mm: minor version
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
short LIB_CALL CPC_getLibVersion(unsigned short* value);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short CPC_listDevices(char** devices, short *number)
/// \brief		List Aurea Technology devices connected
/// \details	List Aurea Technology devices connected
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		**devices pointer to the table buffer which contain list of devices connected \n
///						 Output format: "deviceName - serialNumber" \n
///						 Example:									\n
///						 devices[0]="CPC - SN_xxxxx1xxxxx\r\n"		\n
///						 devices[1]="CPC - SN_xxxxx6xxxxx\r\n"		\n
///
/// \param		*number pointer to the number devices connected
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_listDevices(char** devices, short* number);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short CPC_openDevice(short iDev)
/// \brief		Open and initialize CPC device
/// \details	Open USB connection and initialize internal configuration
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_openDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn	void	short CPC_closeDevice(short iDev)
///	\brief		Close CPC device
/// \details	Close USB connection of previously CPC opened. 
///	\note		Mandory to do after each end of system transfer.
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_closeDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getSystemVersion(short iDev, char *version)
/// \brief		Get system version
/// \details	Get system version: Serial number, product number and firmware version
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		version	Pointer to the buffer which receive the system version. \n
///						String format: SN_'serialNumber':PN_'ProductNumber':'FirmwareVersion'\n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getSystemVersion(short iDev, char *version);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getSystemFeature(short iDev, short iFeature, short* value)
///	\brief		Get system feature
/// \details	Read EEPROM to recovery one feature of system
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		iFeature	0: detection speed 		\n
///							1: module type 			\n
///							2: module performance	\n
///							3: reserved				\n
///							4: reserved				\n
///							5: Output format type			
///
///  \param		*value	pointer to the feature value (format: short)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL CPC_getSystemFeature(short iDev, short iFeature, short* value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getSystemHardwareVersion(short iDev, short card, unsigned short *version, unsigned short *model)
///	\brief		Get system hardware card version and model
///	\details	
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		card	0 : uMCE	\n
///						1 : MTM	\n
///						2 : MCT		
///
/// \param		version	pointer on value of card's version
/// 
/// \param		model	pointer on value of card's model
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL CPC_getSystemHardwareVersion(short iDev, short card, unsigned short* version, unsigned short* model);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setEfficiency(short iDev, short efficiency)
///	\brief		Set efficiency
/// \details	Set APD effciency value (in %).
/// \note		version compatibility : PN_CPC_A_xx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
///	\param		efficiency	Efficiency value in % and in multiple of 5
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
///
short LIB_CALL CPC_setEfficiency(short iDev, short efficiency);



/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getEfficiency(short iDev, short *efficiency)
/// \brief		Get efficiency
/// \details	Get actual APD efficiency value (in %).
/// \note		version compatibility : PN_CPC_A_xx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*efficiency	pointer to the efficiency value (format: double)
///
///	\return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_getEfficiency(short iDev, short *efficiency);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setDeadTime(short iDev, double deadTime)
///	\brief		Set deadtime
/// \details	Set APD deadtime value (in us).
/// \note		version compatibility : PN_CPC_A_xx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
///	\param		deadTime DeadTime value in us (format: double) 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
///
short LIB_CALL CPC_setDeadTime(short iDev, double deadTime);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getDeadTime(short iDev, double *deadTime)
///	\brief		Get deadtime
/// \details	Get actual APD deadtime value (in us).
/// \note		version compatibility : PN_CPC_A_xx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*deadTime	pointer to the deadTime value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
///
short LIB_CALL CPC_getDeadTime(short iDev, double *deadTime);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getCLKCountData(short iDev, unsigned long *CLK, unsigned long *Count)
///	\brief		Get clock and count data
/// \details	Get both clock value and photons count.
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///	
/// \param		CLK	Pointer to the APD clock value (format: decimal)	
/// 
/// \param		Count	Pointer to the APD count value (format: decimal)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getCLKCountData(short iDev, unsigned long *CLK, unsigned long *Count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_saveAllSettings(short iDev)
///	\brief		Save all parameters
/// \details	Save all parameters (deadtime, detection mode, ...).
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_saveAllSettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_factorySettings(short iDev)
///	\brief		System factory settings
/// \details	Set all parameters with factory settings.
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_factorySettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short CPC_setCountingRate(short iDev, double rate)
/// \brief		Set the rate of sending photons counted
/// \details	Set the rate of sending photons counted. Rate value between 0.1s 
///				to 10s. 
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		rate	Rate value in s (format: double) \n 
///						Value between 0.1 to 10.0s with step of 0.1s
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_setCountingRate(short iDev, double rate);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short CPC_getCountingRate(short iDev, double *rate)
/// \brief		Get counting rate value
/// \details	Get the rate of sending photons counted. Rate value between 0.1s 
///				to 10s. 
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param      rate   Pointer to the current rate value (format: double).
///
/// \return	
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_getCountingRate(short iDev, double *rate);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getEfficiencyRange(short iDev, char *range)
///	\brief		Get efficiency range
///	\details	Send all APD efficiency available values
/// \note		version compatibility : PN_CPC_A_xx_xx
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		range	Pointer to the string buffer of the efficiency range.
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getEfficiencyRange(short iDev, char *range);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getDeadTimeRange(short iDev, double *MinVal, double *MaxVal)
///	\brief		Get deadtime range
///	\details	Get min and max values of the deadtime range
/// \note		version compatibility : PN_CPC_A_xx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
///	\param		MinVal	pointer to the current deadtime min value (format: double) 
///
///	\param		MaxVal	pointer to the current deadtime max value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getDeadTimeRange(short iDev, double *MinVal, double *MaxVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_resetSystem(short iDev)
///	\brief		Reset system
///	\details	Reset system
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_resetSystem(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getBodySocketTemp(short iDev, double *bodyTemp)
///	\brief		Get body socket system temperature
///	\details	Get body socket system temperature
/// \note		version compatibility : PN_CPC_A_xx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*bodyTemp	pointer to the body temperature value (format: double)
/// 
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getBodySocketTemp(short iDev, double *bodyTemp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getSystemAlarms(short iDev, char *alarm)
///	\brief		Get System Alarms
///	\details	Get active alarm detected by system
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
///	\param		alarm	Pointer to the buffer which receive the alarm description. \n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				1 : Alarm detected	   \n
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getSystemAlarms(short iDev, char *alarm);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setOutputState(short iDev, short state)
/// \brief		Set output state
///	\details	Set output state
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
///	\param		state	output state: 1=enabled; 0=disabled
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_setOutputState(short iDev, short state);



/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getOutputState(short iDev, short *state)
/// \brief		Get output state
///	\details	Get output state
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*state	pointer to the output state (format: short)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL CPC_getOutputState(short iDev, short *state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setOutputFormat(short iDev, short format)
///	\brief		Set output detection format
///	\details	Set output detection format in numerical, analogic or NIM
/// \note		version compatibility : PN_CPC_A_xx_xx
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		format	0: numerical \n
///						1: analogic \n
///						2: NIM 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_setOutputFormat(short iDev, short format);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getOutputFormat(short iDev, short *format)
///	\brief		Get output detection format 
///	\details	Get output detection format analog, numeric or NIM
/// \note		version compatibility : PN_CPC_A_xx_xx
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*format	pointer to the output format (format: short) \n
/// 					0: numerical \n
///						1: analogic \n
///						2: NIM 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getOutputFormat(short iDev, short *format);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setIntegTime(short iDev, double timeInMs)
///	\brief		Set integration time 
///	\details	Set integration time of detections counted for analog output format
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		timeInMs	integration time between 0.1 to 10000.0 ms
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_setIntegTime(short iDev, double timeInMs);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getIntegTime(short iDev, double* timeInMs)
///	\brief		Get integration time 
///	\details	Get current integration time (in ms) of analog output format 
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*timeInMs	pointer to the time value in ms (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getIntegTime(short iDev, double* timeInMs);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setAnalogOutGain(short iDev, double gain)
///	\brief		Set analog gain  
///	\details	Set gain of analogic format output
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		gain	Analogic gain between 0.1 to 100.0
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_setAnalogOutGain(short iDev, double gain);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getAnalogOutGain(short iDev, double *gain)
///	\brief		Get analog gain
///	\details	Get gain of analogic format output 
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*gain	pointer to the gain value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getAnalogOutGain(short iDev, double *gain);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setDetectionMode(short iDev, short mode)
///	\brief		Set  detection mode 
///	\details	Set detection mode in continuous or gated mode
/// \note		version compatibility : \n
///					PN_CPC_x_Cx_xx  \n
///					PN_CPC_x_Qx_xx, \n
///					PN_CPC_x_Ix_xx, \n
///					PN_CPC_x_Jx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		mode	0: continuous	\n
///						1: gated
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_setDetectionMode(short iDev, short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getDetectionMode(short iDev, short *mode)
///	\brief		Get detection mode 
///	\details	Get detection mode in continuous or gated mode 
/// \note		version compatibility : \n
///					PN_CPC_x_Cx_xx  \n
///					PN_CPC_x_Qx_xx, \n
///					PN_CPC_x_Ix_xx, \n
///					PN_CPC_x_Jx_xx
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*mode	pointer to the detection mode (format: short)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getDetectionMode(short iDev, short *mode);



/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setInputVoltageThreshold(short iDev, double voltage)
///	\brief		Set input voltage threshold 
///	\details	Set voltage threshold for system pulses input
/// \note		version compatibility : PN_CPC_V_xx_xx
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		voltage	Voltage threshold between 0.2 to 4.0 V
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_setInputVoltageThreshold(short iDev, double voltage);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getInputVoltageThreshold(short iDev, double *voltage)
///	\brief		Get input voltage threshold
///	\details	Get voltage threshold of system pulses input
/// \note		version compatibility : PN_CPC_V_xx_xx
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*voltage	pointer to the voltage value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getInputVoltageThreshold(short iDev, double *voltage);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setDetLimitThreshold(short iDev, double detections)
///	\brief		Set detections limit threshold 
///	\details	Set detections limit threshold after which the system power off 
///				the detector to protect it.
/// \note		version compatibility : all
/// 
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		detections	Number of photons detected between 0.1 to 25.0 M photons/s
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_setDetLimitThreshold(short iDev, double detections);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getDetLimitThreshold(short iDev, double* detections)
///	\brief		Get detections limit threshold
///	\details	Get detections limit threshold after which the system power off 
///				the detector to protect it.
/// \note		version compatibility : all
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \param		*detections	pointer to the detection value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getDetLimitThreshold(short iDev, double* detections);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getOutputVoltage(short iDev, double *voltage)
///	\brief		Get output voltage
/// \details	Get the current output voltage in analog format output
/// \note		version compatibility : all
///		
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*voltage	Pointer to the voltage value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL CPC_getOutputVoltage(short iDev, double *voltage);



/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setFanState(short iDev, short state)
///	\brief		Set Fan state
///	\details	Set Fan On or Off
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		state	0 : OFF, 1 : ON
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL CPC_setFanState(short iDev, short state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getFanState(short iDev, short *state)
///	\brief		Get Fan state
///	\details	Get Fan On or Off
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*state	pointer to the fan state (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL CPC_getFanState(short iDev, short* state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_setLedState(short iDev, short state)
///	\brief		Set Led state
///	\details	Set Led On or Off
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		state	0 : OFF, 1 : ON
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL CPC_setLedState(short iDev, short state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_getLedState(short iDev, short *state)
///	\brief		Get Led state
///	\details	Get Led On or Off
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// 
/// \param		*state	pointer to the Led state (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL CPC_getLedState(short iDev, short* state);

/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short CPC_identify(short iDev)
///	\brief		Identify the device
///	\details	make device led blink
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL CPC_identify(short iDev);

#if defined(__cplusplus)
}
#endif

#endif
